<?php


namespace Kowal\Magento2ProductFeedForB2b\Model\Config\Source;

class Storeid implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
    }

    public function toOptionArray()
    {
        return $this->getStoreData('OptionArray');
//        return [['value' => '1', 'label' => __('Store ID 1')], ['value' => '2', 'label' => __('Store ID 2')]];
    }

    public function toArray()
    {
        return $this->getStoreData('Array');
//        return ['1' => __('Store ID 1'), '2' => __('Store ID 2')];
    }

    private function getStoreData($type = "OptionArray")
    {
        $storeManagerDataList = $this->_storeManager->getStores();
        $options = array();

        switch ($type) {
            case "OptionArray":
                foreach ($storeManagerDataList as $key => $value) {
                    $options[] = ['value' => $key, 'label' => $key . ' - ' . $value['name'] . ' - ' . $value['code']];
                }
                break;

            case "Array":
                foreach ($storeManagerDataList as $key => $value) {
                    $options[] = [$key => $key . ' - ' . $value['name'] . ' - ' . $value['code']];
                }
                break;
        }

        return $options;
    }
}
