<?php


namespace Kowal\Magento2ProductFeedForB2b\Cron;

class B2bfeed
{

    protected $logger;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Kowal\Magento2ProductFeedForB2b\lib\Feed $feed
    )
    {
        $this->logger = $logger;
        $this->feed = $feed;

        parent::__construct();
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->addInfo("Start");
        $this->feed->run();
        $this->logger->addInfo("Koniec");
        return true;
    }
}
